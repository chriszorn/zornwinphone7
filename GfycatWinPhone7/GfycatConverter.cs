﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GfycatWinPhone7
{
    public class GfycatConverter
    {
        public async Task<string> Convert(string gifUrl)
        {
            System.Diagnostics.Debug.WriteLine(gifUrl);
            string mp4Url = null;
            JsonTranscodeResponse conversionResponse;
            //string gifIdentifier = CreateUploadIdentifier();

            //conversionResponse = await GetConversionResponse(gifUrl, gifIdentifier);
            //mp4Url = GetMp4Url(conversionResponse);

            //if (!string.IsNullOrEmpty(mp4Url))
            //{
            //    return mp4Url;
            //}

            //if (conversionResponse != null && conversionResponse.IsVideoProcessing)
            //{
                do
                {
                    conversionResponse = await GetConversionResponse(gifUrl, CreateUploadIdentifier());
                    mp4Url = GetMp4Url(conversionResponse);
                    if (!string.IsNullOrEmpty(mp4Url))
                    {
                        System.Diagnostics.Debug.WriteLine(gifUrl + " " + mp4Url);
                        return mp4Url;
                    }
                }
                while (conversionResponse.IsVideoProcessing ||
                    conversionResponse.Error == JsonTranscodeResponse.ConversionInProgress ||
                    conversionResponse.Error == JsonTranscodeResponse.IdentifierInUse);
            //}

            return null;
        }

        private async Task<JsonStatusResponse> GetMp4UrlStatus(string gifHash)
        {
            string statusUrl = string.Format("http://tracking.gfycat.com/status/{0}", gifHash);
            string resp = await HttpGetRequest(statusUrl);
            //System.Diagnostics.Debug.WriteLine(resp);
            return JsonConvert.DeserializeObject<JsonStatusResponse>(resp);
        }

        private async Task<JsonTranscodeResponse> GetConversionResponse(string gifUrl, string gifHash)
        {
            string initUrl = string.Format("http://gfycat.com/ajax/initiate/{0}", gifHash);

            string resp = await HttpGetRequest(initUrl);
            //System.Diagnostics.Debug.WriteLine(resp);

            string transcodeReleaseUrl = string.Format(
                "http://upload.gfycat.com/transcodeRelease/{0}?immediatePublish=true&fetchUrl={1}",
                gifHash,
                HttpUtility.UrlEncode(gifUrl));

            resp = await HttpGetRequest(transcodeReleaseUrl);
            //System.Diagnostics.Debug.WriteLine(resp);

            if (string.IsNullOrWhiteSpace(resp))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<JsonTranscodeResponse>(resp);
        }

        private string GetMp4Url(JsonTranscodeResponse transcodeRespObj)
        {
            if (transcodeRespObj != null && !string.IsNullOrEmpty(transcodeRespObj.Mp4Url))
            {
                return transcodeRespObj.Mp4Url;
            }

            return null;
        }

        private static string CreateUploadIdentifier()
        {
            const string s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var c = new char[5];
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = s[RandomNumber(0, s.Length)];
            }
            return new string(c) + ".gif";
        }

        public static async Task<string> HttpGetRequest(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return default(string);
            }
        }

        //Function to get random number
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }
    }
}
