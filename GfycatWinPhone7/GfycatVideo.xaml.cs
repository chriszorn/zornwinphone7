﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace GfycatWinPhone7
{
    public partial class GfycatVideo : UserControl
    {
        private static readonly DependencyProperty _gifSource = DependencyProperty.Register(
          "GifSource", typeof(string), typeof(GfycatVideo), new PropertyMetadata(null, GifSourcePropertyChanged));

        public string GifSource
        {
            get { return (string)GetValue(_gifSource); }
            set { SetValue(_gifSource, value); }
        }

        private static void GifSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((string)e.NewValue != "GfycatWinPhone7.GfycatVideo+GfycatVideoViewModel")
            {
                (d as GfycatVideo).Update();
            }
        }

        private GfycatConverter converter = new GfycatConverter();
        //private GfycatVideoViewModel viewModel = new GfycatVideoViewModel();

        public GfycatVideo()
        {
            InitializeComponent();

            //DataContext = viewModel;
            //DataContext = this;
        }

        //private string _videoSource = null;
        //public string VideoSource
        //{
        //    get { return _videoSource; }
        //    set { _videoSource = value; NotifyPropertyChanged(); }
        //}

        public async void Update()
        {
            string mp4Url = await converter.Convert(GifSource);

            if (string.IsNullOrWhiteSpace(mp4Url))
            {
                LoadingTextBlock.Text = "Error loading content";
            }
            else
            {
                VideoMediaElement.AutoPlay = false;
                VideoMediaElement.Source = null;
                VideoMediaElement.Source = new Uri(mp4Url, UriKind.Absolute);
                //VideoMediaElement.Play();
                LoadingTextBlock.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as System.Windows.Controls.MediaElement).Play();
        }

        private void GfycatVideoLayoutRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //VideoMediaElement.AutoPlay = true;
            //Update();
            VideoMediaElement.Play();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged([CallerMemberName] String info = "")
        {
            if (PropertyChanged != null)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
                catch { }
            }
        }

        private void VideoMediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            //(sender as System.Windows.Controls.MediaElement).Play();
        }

        private void VideoMediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            Update();
        }
    }

    //public class GfycatVideoViewModel : INotifyPropertyChanged
    //{
    //    private string _videoSource = null;
    //    public string VideoSource
    //    {
    //        get { return _videoSource; }
    //        set { _videoSource = value; NotifyPropertyChanged(); }
    //    }

    //    public event PropertyChangedEventHandler PropertyChanged;
    //    public void NotifyPropertyChanged([CallerMemberName] String info = "")
    //    {
    //        if (PropertyChanged != null)
    //        {
    //            try
    //            {
    //                PropertyChanged(this, new PropertyChangedEventArgs(info));
    //            }
    //            catch { }
    //        }
    //    }
    //}
}
