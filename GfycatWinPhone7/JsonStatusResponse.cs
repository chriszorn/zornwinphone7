﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace GfycatWinPhone7
{
    public class JsonStatusResponse
    {
        [JsonProperty("task")]
        public string Task { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }
    }
}
