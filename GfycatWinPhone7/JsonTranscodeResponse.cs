﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace GfycatWinPhone7
{
    public class JsonTranscodeResponse
    {
        public static readonly string ConversionInProgress = "Url conversion already in progress.";
        public static readonly string IdentifierInUse = "Key already in use.  Please choose unique key or do not specify key.";

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("isOk")]
        public bool IsVideoProcessing { get; set; }

        [JsonProperty("mp4Url")]
        public string Mp4Url { get; set; }
    }
}
