﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace TemplateApp.Shared
{
    public static class App
    {
        public static BitmapImage ToastImage = new BitmapImage(new Uri("/Battlelog.Shared;component/Assets/ToastApplicationIcon.png", UriKind.Relative));

        public static async void ApplicationLaunching()
        {
            string status = await ZornWinPhone7.ApplicationHelper.CheckAppStatus(
                WebService.StatusUrl,
                Settings.LastServerStatus.Value);
            Settings.LastServerStatus.Value = status;
        }

        public static void LogException(Exception e)
        {
            ZornWinPhone7.LittleWatson.ReportException(e, "Unhandled Exception Error");
        }

        public static void OnNavigateToFirstPage()
        {
            ZornWinPhone7.LittleWatson.AppName = string.Format(
                "{0} v{1}",
                TemplateApp.Shared.Resources.AppResources.ApplicationTitle,
                TemplateApp.Shared.Resources.AppResources.Version);

            ZornWinPhone7.LittleWatson.CheckForPreviousException();
        }
    }
}
