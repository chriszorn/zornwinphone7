﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZornWinPhone7;

namespace TemplateApp.Shared
{
    public static class NavigationHelper
    {
        public static void NavigateToAboutView()
        {
            ZornWinPhone7.About.DataContextArg = new ZornWinPhone7.ViewModels.AboutViewModel()
            {
                AboutItems = new ObservableCollection<AboutItem>()
                    {
                        new AboutItem() 
                        { 
                            Version = "Version 1.0.0.0", 
                            Text = "- Initial Release"
                        },
                    },
                ApplicationTitle = TemplateApp.Shared.Resources.AppResources.ApplicationTitle,
                Version = TemplateApp.Shared.Resources.AppResources.Version,
            };

            ZornWinPhone7.NavigationHelper.NavigatToAboutPage();
        }

        public static void NavigateToSample(object param)
        {
            if (param != null)
            {
                ViewArgs.Sample_ObjectArg = param;
                ZornWinPhone7.NavigationHelper.Navigate(new Uri("/Views/Sample.xaml", UriKind.Relative));
            }
        }
    }
}
