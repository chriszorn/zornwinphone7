
To alter for a new app

Hand rename folders:
	./TemplateApp.Shared/
	./TemplateApp.WP7/
	./TemplateApp.WP8/

Hand rename files:
	./TemplateApp.Shared/TemplateApp.Shared.csproj
	./TemplateApp.WP7/TemplateApp.WP7.csproj
	./TemplateApp.WP8/TemplateApp.WP8.csproj

Open renamed .csproj files and replace "TemplateApp" with <AppName>

Open solution and replace "TemplateApp" with <AppName> in Entire Solution

