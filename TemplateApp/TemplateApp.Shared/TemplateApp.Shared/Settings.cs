﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsPhoneApp;

namespace TemplateApp.Shared
{
    public static class Settings
    {
        public static readonly Setting<string> LastServerStatus = new Setting<string>("LastServerStatus", string.Empty);
    }
}
