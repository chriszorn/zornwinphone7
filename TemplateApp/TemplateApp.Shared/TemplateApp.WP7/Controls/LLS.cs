﻿using System.Windows;
using Microsoft.Phone.Controls;

namespace TemplateApp.Controls
{
    /// <summary>
    /// The WP7 version
    /// </summary>
    public class LLS : LongListSelector
    {
        public delegate void MyItemRealizedEventHandler(object sender, object dataContext);

        public event MyItemRealizedEventHandler MyItemRealized;

        public LLS()
        {
            IsFlatList = true;
            Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Transparent);
            this.Link += LLS_Link;
        }

        void LLS_Link(object sender, LinkUnlinkEventArgs e)
        {
            if (MyItemRealized != null)
            {
                MyItemRealized(sender, e.ContentPresenter.Content);
            }
        }
    }

    public class LLSGrouped : LongListSelector
    {
        public LLSGrouped()
        {
            IsFlatList = false;
            Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Transparent);
        }
    }
}
