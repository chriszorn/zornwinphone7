﻿using Microsoft.Phone.Controls;

namespace TemplateApp.Controls
{
    /// <summary>
    /// The WP8 Version
    /// </summary>
    public class LLS : LongListSelector
    {
        public delegate void MyItemRealizedEventHandler(object sender, object dataContext);

        public event MyItemRealizedEventHandler MyItemRealized;

        public LLS()
        {
            this.ItemRealized += LLS_ItemRealized;
        }

        void LLS_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            if (MyItemRealized != null)
            {
                MyItemRealized(sender, e.Container.DataContext);
            }
        }
    }

    public class LLSGrouped : LongListSelector
    {
        public LLSGrouped()
        {
            IsGroupingEnabled = true;
        }
    }
}
