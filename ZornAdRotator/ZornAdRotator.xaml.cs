﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornAdRotator
{
    public partial class ZornAdRotator : UserControl
    {
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("AdMobId", typeof(string), typeof(ZornAdRotator), new PropertyMetadata(default(string), AdMobIdPropertyChanged));

        public string AdMobId
        {
            get { return (string)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        private static void AdMobIdPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var adRotator = (ZornAdRotator)dependencyObject;
            adRotator.AdMobAd.AdUnitID = adRotator.AdMobId;
        }

        public ZornAdRotator()
        {
            InitializeComponent();
            //DefaultAd.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void DefaultAd_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                Uri appUri = new Uri("http://www.windowsphone.com/en-us/store/app/pin-it-pinterest/7aad351c-093d-494e-be80-0d052a078145");
                Microsoft.Phone.Tasks.WebBrowserTask browserTask = new Microsoft.Phone.Tasks.WebBrowserTask();
                browserTask.Uri = appUri;
                browserTask.Show();
            }
            catch { }
        }

        private void AdMobAd_FailedToReceiveAd(object sender, GoogleAds.AdErrorEventArgs e)
        {
            DefaultAdGrid.Visibility = System.Windows.Visibility.Visible;
            //AdMobAdGrid.Visibility = System.Windows.Visibility.Collapsed;
            System.Diagnostics.Debug.WriteLine("[Warning] AdMobAd_FailedToReceiveAd {0}", e.ErrorCode);
        }

        private void AdMobAd_ReceivedAd(object sender, GoogleAds.AdEventArgs e)
        {
            DefaultAdGrid.Visibility = System.Windows.Visibility.Collapsed;
            AdMobAdGrid.Visibility = System.Windows.Visibility.Visible;
        }
    }
}
