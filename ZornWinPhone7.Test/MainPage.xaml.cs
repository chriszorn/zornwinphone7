﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7.Test
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            ZornWinPhone7.ReviewPrompt.OnAppOpen();

            ApplicationBar = new ApplicationBar();

            var about = new ApplicationBarMenuItem() { Text = "about" };
            ApplicationBar.MenuItems.Add(about);
            about.Click += (sender, e) =>
            {
                ZornWinPhone7.About.DataContextArg = new ZornWinPhone7.ViewModels.AboutViewModel()
                {
                    AboutItems = new ObservableCollection<AboutItem>()
                    {
                        new AboutItem() 
                        { 
                            Version = "Version 1.0.0.0", 
                            Text = "- Initial Release"
                        },
                    },
                    ApplicationTitle = "ApplicationTitle",
                    Version = "1.0.0.0",
                };
                ZornWinPhone7.NavigationHelper.NavigatToAboutPage();
            };
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //await GfycatWinPhone7.GfycatConverter.Convert("x");

            ZornWinPhone7.ApplicationHelper.CheckForAppUpdate(
                "http://www.windowsphone.com/en-us/store/app/complete-league-of-legends/88b11e28-c176-40fc-980d-5b6808d9c4cb",
                string.Join(".", "5.5.0.0".Split(new char[] { '.' }).Take(3)));

            LittleWatson.CheckForPreviousException();

            for (int i = 0; i < 90; i++)
            {
                string x = "";
                for (int j = 0; j < 100; j++)
                {
                    x += "Z";
                }
                LittleWatson.Log(x);
            }

            //throw new DivideByZeroException();
        }

        private void Test_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //ZornWinPhone7.ImagePage.ImageSourceUriArg = new Uri("http://cdn.leagueoflegends.com/game-info/1.0.19/images/champion/backdrop/bg-ahri.jpg", UriKind.Absolute);
            NavigationService.Navigate(new Uri(((sender as Grid).DataContext as AppMenuItem).DestinationString, UriKind.Relative));
        }
    }
}