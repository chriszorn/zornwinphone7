﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7.Test
{
    public partial class ColorPicker : PhoneApplicationPage
    {
        public ColorPicker()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }
    }

    public class TestColorPickerViewModel : ZornWinPhone7.ViewModelBase
    {
        private SolidColorBrush _testFillBinding = new SolidColorBrush(Colors.Orange);
        public SolidColorBrush TestFillBinding
        {
            get { return _testFillBinding; }
            set { _testFillBinding = value; NotifyPropertyChanged(); }
        }

        private string _myHeader = "x";
        public string MyHeader
        {
            get { return _myHeader; }
            set { _myHeader = value; NotifyPropertyChanged(); }
        }

        public TestColorPickerViewModel()
        {
            TestFillBinding = new SolidColorBrush(Colors.Orange);
            MyHeader = "Header Color";
        }
    }
}