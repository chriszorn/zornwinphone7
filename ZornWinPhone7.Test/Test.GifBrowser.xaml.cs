﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7.Test
{
    public partial class GifBrowser : PhoneApplicationPage
    {
        public GifBrowser()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //string url = "http://www.reactiongifs.com/wp-content/uploads/2013/03/ajoker.gif";
            ////Browser.NavigateToString(string.Format("<html><head><script type=\"text/javascript\">window.onload = function () { var elem = document.getElementById('content'); window.external.Notify(elem.scrollHeight + ''); }</script></head><body><img src=\"{0}\" style=\"width:100%\" /></body></html>", url));
            //Browser.NavigateToString("<html><head><style>body, div, img { margin:0; padding:0; }</style><script type=\"text/javascript\">window.onload = function () { var elem = document.getElementById('content'); window.external.Notify(elem.scrollHeight + ''); document.getElementById('body').removeChild(document.getElementById('content')); }</script></head><body id=\"body\"><div id=\"content\" style=\"width:100px\"><img src=\"http://www.reactiongifs.com/wp-content/uploads/2013/03/joker.gif\" style=\"width:100%\" /></div><img src=\"http://www.reactiongifs.com/wp-content/uploads/2013/03/joker.gif\" style=\"width:100%\" /></body></html>");
        }

        private void Browser_ScriptNotify(object sender, NotifyEventArgs e)
        {
            WebBrowser thisBrowser = (WebBrowser)sender;
            int height = Convert.ToInt32(e.Value);
            if (height > 0)
            {
                double newHeight = thisBrowser.ActualWidth * (height / 100.0);
                thisBrowser.Height = newHeight - 2;
            }
        }

        private void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            (sender as WebBrowser).NavigateToString("<html><head><style>body, div, img { margin:0; padding:0; }</style><script type=\"text/javascript\">window.onload = function () { var elem = document.getElementById('content'); var height = 0; if(elem != null) { height = elem.scrollHeight; document.getElementById('body').removeChild(document.getElementById('content')); } window.external.Notify(height + ''); }</script></head><body id=\"body\"><div id=\"unloaded-content\" style=\"width:100px; max-height:10px; overflow:hidden;\"><img src=\"http://www.reactiongifs.com/wp-content/uploads/2013/03/joker.gif\" style=\"width:100%\" onload=\"document.getElementById('unloaded-content').id = 'content';\" /></div><img src=\"http://www.reactiongifs.com/wp-content/uploads/2013/03/joker.gif\" style=\"width:100%\" /></body></html>");
        }
    }
}