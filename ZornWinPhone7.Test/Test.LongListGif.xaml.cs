﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ZornWinPhone7.Controls;

namespace ZornWinPhone7.Test
{
    public partial class LongListGif : PhoneApplicationPage
    {
        public LongListGif()
        {
            InitializeComponent();
            ImageTools.IO.Decoders.AddDecoder<ImageTools.IO.Gif.GifDecoder>();
        }

        private void AnimatedImage_Unloaded(object sender, RoutedEventArgs e)
        {
            var ai = (sender as ImageTools.Controls.AnimatedImage);
            System.Diagnostics.Debug.WriteLine("unLoad ");

            ai.Stop();
            sender = null;
            sender = new ImageTools.Controls.AnimatedImage();
        }

        private void RemoteGifImage_Loaded(object sender, RoutedEventArgs e)
        {
            var ri = sender as RemoteGifImage;
            string imageUrl = ri.ImageUrl;
            GifListViewModel dc = DataContext as GifListViewModel;
            System.Diagnostics.Debug.WriteLine("Load " + Array.IndexOf(dc.GifUrls, imageUrl));
        }

        private void RemoteGifImage_Unloaded(object sender, RoutedEventArgs e)
        {

            var ri = sender as RemoteGifImage;
            string imageUrl = ri.ImageUrl;
            GifListViewModel dc = DataContext as GifListViewModel;
            System.Diagnostics.Debug.WriteLine("UnLoad " + Array.IndexOf(dc.GifUrls, imageUrl));
        }
    }
}