﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZornWinPhone7.UnitTest
{
    [TestClass]
    public class IEnumerableTest
    {
        [TestMethod]
        public void UpdateSortedObservableCollectionTest1()
        {
            ObservableCollection<IUpdateable> items = new ObservableCollection<IUpdateable>();
            items.Add(new UpdateableObject() { Id = "1", Text = "A" });
            items.Add(new UpdateableObject() { Id = "2", Text = "B" });
            items.Add(new UpdateableObject() { Id = "3", Text = "C" });

            ObservableCollection<IUpdateable> newItems = new ObservableCollection<IUpdateable>();
            newItems.Add(new UpdateableObject() { Id = "2", Text = "B" });
            newItems.Add(new UpdateableObject() { Id = "1", Text = "A1" });
            newItems.Add(new UpdateableObject() { Id = "3", Text = "C1" });

            IEnumerableUtil.UpdateSortedObservableCollection(ref items, ref newItems);

            for (int i = 0; i < newItems.Count; i++)
            {
                Assert.AreEqual(((UpdateableObject)items[i]).Text, ((UpdateableObject)newItems[i]).Text);
            }
        }
    }

    public class UpdateableObject : IUpdateable
    {
        public string Id { get; set; }
        public string Text { get; set; }

        public override bool Equals(object obj)
        {
            return (obj as UpdateableObject).Id.Equals(Id);
        }

        public void Update(object other)
        {
            var x = (UpdateableObject)other;
            Text = x.Text;
        }
    }
}
