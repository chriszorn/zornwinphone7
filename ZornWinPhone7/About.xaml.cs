﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace ZornWinPhone7
{
    public partial class About : PhoneApplicationPage
    {
        public static ViewModels.AboutViewModel DataContextArg;

        public About()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (DataContextArg != null)
            {
                DataContext = DataContextArg;
                DataContextArg = null;
            }
        }

        private void App_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = ((sender as Grid).DataContext as AppMenuItem).Destination;
            webBrowserTask.Show();
        }

        private void Credit_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var appMenuItem = ZornWinPhone7.VisualTreeUtil.CastObjectDataContext<AppMenuItem>(sender);
            if (appMenuItem != null)
            {
                WebPageDestination(appMenuItem.Destination);
            }
        }

        private void Email_Tap()
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.To = "chris.zorn@live.com";
            emailComposeTask.Body = "";
            emailComposeTask.Subject = string.Format(
                "Windows Phone App Support: {0} v{1}",
                (DataContext as ViewModels.AboutViewModel).ApplicationTitle,
                (DataContext as ViewModels.AboutViewModel).Version);
            emailComposeTask.Show();
        }

        private void Option_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var appMenuItem = ZornWinPhone7.VisualTreeUtil.CastObjectDataContext<AppMenuItem>(sender);

            if (appMenuItem == ZornWinPhone7.ViewModels.AboutViewModel.RateItem)
            {
                Rate_Tap();
            }
            else if (appMenuItem == ZornWinPhone7.ViewModels.AboutViewModel.EmailItem)
            {
                Email_Tap();
            }
            else
            {
                if (appMenuItem != null)
                {
                    WebPageDestination(appMenuItem.Destination);
                }
            }
        }

        private void Rate_Tap()
        {
            (new MarketplaceReviewTask()).Show();            
        }

        private void WebPageDestination(Uri uri)
        {
            ZornWinPhone7.Tasks.OpenInBrowser(uri);
        }

        private void BugReportButton_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as ZornWinPhone7.ViewModels.AboutViewModel).BugReportButton_Click();
        }
    }
}