﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ZornWinPhone7
{
    [DataContract]
    public class AboutItem : ModelBase
    {
        private string _version;
        [DataMember]
        public string Version
        {
            get { return _version; }
            set { _version = value; NotifyPropertyChanged(); }
        }

        private string _text;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged(); }
        }
    }
}
