﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ZornWinPhone7
{
    public class AppMenuItem : ModelBase
    {
        private string _title;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(); }
        }

        private string _text;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged(); }
        }

        private string _iconImage;
        [DataMember]
        public string IconImage
        {
            get { return _iconImage; }
            set { _iconImage = value; NotifyPropertyChanged(); }
        }

        [DataMember]
        public Uri Destination;

        private string _destinationString;
        [DataMember]
        public string DestinationString
        {
            get { return _destinationString; }
            set { _destinationString = value; NotifyPropertyChanged(); }
        }
    }
}
