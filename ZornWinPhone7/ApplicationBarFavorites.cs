﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7
{
    public class ApplicationBarFavorites
    {
        private bool _isFavorited;
        public bool IsFavorited
        {
            get { return _isFavorited; }
            set { _isFavorited = value; OnIsFavoriteChanged(); }
        }

        public delegate void AddFavoritesClickEventHandler(object sender, EventArgs e);
        public event AddFavoritesClickEventHandler AddFavoritesClick;

        public delegate void RemoveFavoritesClickEventHandler(object sender, EventArgs e);
        public event RemoveFavoritesClickEventHandler RemoveFavoritesClick;

        private ApplicationBar appBar;
        private ApplicationBarIconButton addFavoritesButton;
        private ApplicationBarIconButton removeFavoritesButton;

        public ApplicationBarFavorites(ApplicationBar appBar)
        {
            this.appBar = appBar;
            addFavoritesButton = new ApplicationBarIconButton()
            {
                Text = "favorite",
                IconUri = new Uri("/Assets/Icons/ZWP7.Addfavorite.png", UriKind.Relative)
            };

            removeFavoritesButton = new ApplicationBarIconButton()
            {
                Text = "unfavorite",
                IconUri = new Uri("/Assets/Icons/ZWP7.Deletefavorite.png", UriKind.Relative)
            };

            addFavoritesButton.Click += addFavoritesButton_Click;
            removeFavoritesButton.Click += removeFavoritesButton_Click;

            IsFavorited = false;
        }

        void OnIsFavoriteChanged()
        {
            if (appBar.Buttons.Contains(addFavoritesButton))
            {
                appBar.Buttons.Remove(addFavoritesButton);
            }

            if (appBar.Buttons.Contains(removeFavoritesButton))
            {
                appBar.Buttons.Remove(removeFavoritesButton);
            }

            if (IsFavorited)
            {
                appBar.Buttons.Add(removeFavoritesButton);
            }
            else
            {
                appBar.Buttons.Add(addFavoritesButton);
            }
        }

        void removeFavoritesButton_Click(object sender, EventArgs e)
        {
            if (RemoveFavoritesClick != null)
            {
                RemoveFavoritesClick(sender, e);
            }

            IsFavorited = false;
        }

        void addFavoritesButton_Click(object sender, EventArgs e)
        {
            if (AddFavoritesClick != null)
            {
                AddFavoritesClick(sender, e);
            }

            IsFavorited = true;
        }
    }
}
