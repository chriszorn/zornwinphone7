﻿using Microsoft.Phone.Controls;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using HtmlAgilityPack;
using ZornWinPhone7.Net;

namespace ZornWinPhone7
{
    public static class ApplicationHelper
    {
        public static async Task<string> CheckAppStatus(string url, string lastStatus)
        {
            string status = await NetUtil.HttpGetRequest(url);

            if (string.IsNullOrWhiteSpace(status))
            {
                return string.Empty;
            }

            if (status != lastStatus)
            {
                status = HttpUtility.HtmlDecode(status);

                ApplicationHelper.SafeMessageBox(status, "Attention", MessageBoxButton.OK);
            }

            return status;
        }

        public static void CheckForAppUpdate(string appStoreUrl, string version)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(async(state) => {
                string html = await ZornWinPhone7.Net.NetUtil.HttpGetRequest(appStoreUrl);

                if (!string.IsNullOrWhiteSpace(html))
                {
                    try
                    {
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(html);

                        var versionObj = doc.DocumentNode.SelectSingleNode("//*[contains(@itemprop,'softwareVersion')]");

                        if (versionObj != null)
                        {
                            string appVersion = versionObj.InnerText;
                            int[] appVersionParts = appVersion.Split(new char[] { '.' }).Select(x => Convert.ToInt32(x)).ToArray();
                            int[] versionParts = version.Split(new char[] { '.' }).Select(x => Convert.ToInt32(x)).ToArray();
                            bool existsUpdate = false;

                            for (int i = 0; i < versionParts.Length && i < appVersionParts.Length; i++)
                            {
                                if (appVersionParts[i] < versionParts[i])
                                {
                                    break;
                                }
                                else if (appVersionParts[i] > versionParts[i])
                                {
                                    existsUpdate = true;
                                    break;
                                }
                            }

                            if (existsUpdate)
                            {
                                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                                    {
                                        if (System.Windows.MessageBox.Show("An update is available. Click OK to download", "Update", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                                        {
                                            ZornWinPhone7.Tasks.OpenInBrowser(appStoreUrl);
                                        }
                                    });
                            }
                        }
                    }
                    catch { }
                }
            });
        }

        public static void SafeMessageBox(string text, string title, MessageBoxButton button)
        {
            try
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    try
                    {
                        MessageBox.Show(text, title, button);
                    }
                    catch { }
                });
            }
            catch { }
        }

        public static string ReadStringFromResource(Uri uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("uri", "uri cannot be null");
            }

            var resource = Application.GetResourceStream(uri);

            if (resource == null)
            {
                throw new FileNotFoundException(string.Format("No resource found for {0}", uri.ToString()));
            }

            StreamReader streamReader = new StreamReader(resource.Stream);
            return streamReader.ReadToEnd();
        }

        public static void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as TextBox).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }
    }
}
