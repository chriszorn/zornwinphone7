﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Phone.Controls;

namespace ZornWinPhone7
{
    public static class ContextMenuHelper
    {
        public static void OpenContextMenu(object contextMenuContainer)
        {
            ContextMenuService.GetContextMenu((DependencyObject)contextMenuContainer).IsOpen = true;
        }
    }
}
