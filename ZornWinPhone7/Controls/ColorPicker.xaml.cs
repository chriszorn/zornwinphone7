﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7.Controls
{
    public partial class ColorPicker : UserControl
    {
        private static SolidColorBrush defaultFillBrush = new SolidColorBrush(Colors.Red);

        private static readonly DependencyProperty _fillProperty = DependencyProperty.Register
        (
          "Fill", typeof(SolidColorBrush), typeof(ColorPicker), new PropertyMetadata(defaultFillBrush, PropertyChanged)
        );

        public SolidColorBrush Fill
        {
            get { return (SolidColorBrush)GetValue(_fillProperty); }
            set { SetValue(_fillProperty, value); }
        }

        private static readonly DependencyProperty _headerProperty = DependencyProperty.Register
        (
          "Header", typeof(string), typeof(ColorPicker), new PropertyMetadata(string.Empty, PropertyChanged)
        );

        public string Header
        {
            get { return (string)GetValue(_headerProperty); }
            set { SetValue(_headerProperty, value); }
        }

        private static void PropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            //var control = (ColorPicker)sender;
            //control.Update();
        }

        private void Update()
        {
            //dataContext.Fill = Fill;
            //dataContext.Header = Header;
        }













        public ColorPicker ExposedDataContext
        {
            get { return LayoutRoot1.DataContext as ColorPicker; }
        }

        public ColorPicker()
        {
            InitializeComponent();

            LayoutRoot1.DataContext = this;

            Update();
        }

        void ColorPicker_Navigated(object sender, NavigationEventArgs e)
        {
            var colorPickerPage = e.Content as ColorPickerPage;

            if (colorPickerPage != null)
            {
                colorPickerPage.DataContext = ExposedDataContext;
            }

            var frame = Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame;
            if (frame != null)
            {
                frame.Navigated -= ColorPicker_Navigated;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var frame = Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame;
            if (frame != null)
            {
                frame.Navigated += ColorPicker_Navigated;
            }

            ZornWinPhone7.NavigationHelper.Navigate(new Uri("/ZornWinPhone7;component/ColorPickerPage.xaml", UriKind.Relative));
        }
    }
}
