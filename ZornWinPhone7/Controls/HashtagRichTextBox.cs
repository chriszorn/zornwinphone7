﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace ZornWinPhone7.Controls
{
    public class HashtagRichTextBox : RichTextBox
    {

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(string), typeof(RichTextBox), new PropertyMetadata(null, ContentPropertyChanged));

        public string Content
        {
            get { return (string)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty HashtagForegroundProperty =
            DependencyProperty.Register("HashtagForeground", typeof(SolidColorBrush), typeof(RichTextBox), new PropertyMetadata(null, HashtagForegroundPropertyChanged));

        public SolidColorBrush HashtagForeground
        {
            get { return (SolidColorBrush)GetValue(HashtagForegroundProperty); }
            set { SetValue(HashtagForegroundProperty, value); }
        }

        public delegate void HashtagClickEventHandler(object sender, HashtagClickEventArgs e);

        public event HashtagClickEventHandler HashtagClick;

        private static void HashtagForegroundPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HashtagRichTextBox)dependencyObject;
            var foreground = dependencyPropertyChangedEventArgs.NewValue as SolidColorBrush;

            Update(richTextBox);
        }

        private static void ContentPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HashtagRichTextBox)dependencyObject;
            string content = dependencyPropertyChangedEventArgs.NewValue as string;

            Update(richTextBox);
        }

        public HashtagRichTextBox()
        {
            Update(this);
        }

        private static void Update(HashtagRichTextBox richTextBox)
        {
            richTextBox.Blocks.Clear();
            richTextBox.Blocks.Add(CreateHashtagParagraph(richTextBox));
        }

        private static Block CreateHashtagParagraph(HashtagRichTextBox richTextBox)
        {
            Paragraph paragraph = new Paragraph();

            string content = richTextBox.Content;
            SolidColorBrush hashtagForeground = richTextBox.HashtagForeground;

            if (content != null)
            {
                string[] words = content.Split(new char[] { ' ' });

                for (int i = 0; i < words.Length; i++)
                {
                    string word = words[i];

                    Inline inline = null;

                    if (word.Length >= 2 && word[0] == '#')
                    {
                        Hyperlink hyperlink = new Hyperlink()
                            {
                                FontWeight = System.Windows.FontWeights.Bold,
                                MouseOverTextDecorations = null,
                                TextDecorations = null,
                            };

                        if (i > 0)
                        {
                            paragraph.Inlines.Add(new Run() { Text = " " });
                        }

                        if (hashtagForeground != null)
                        {
                            hyperlink.Foreground = hashtagForeground;
                            hyperlink.MouseOverForeground = new SolidColorBrush() { Color = ZornWinPhone7.ColorUtil.ChangeColorBrightness(hashtagForeground.Color, 0.4F) };
                        }

                        hyperlink.Inlines.Add(new Run() { Text = word });
                        hyperlink.Click += richTextBox.HashtagRichTextBoxHyperlink_Click;

                        inline = hyperlink;
                    }
                    else
                    {
                        if (i > 0)
                        {
                            word = string.Format(" {0}", word);
                        }

                        inline = new Run()
                        {
                            Text = word
                        };
                    }

                    if (inline != null)
                    {
                        paragraph.Inlines.Add(inline);
                    }
                }
            }

            return paragraph;
        }

        void HashtagRichTextBoxHyperlink_Click(object sender, RoutedEventArgs e)
        {
            if (HashtagClick != null)
            {
                //HashtagClick(this, sender as System.Windows.Documents.Hyperlink, ((sender as System.Windows.Documents.Hyperlink).Inlines.Last() as Run).Text);
                HashtagClick(
                    this, 
                    new HashtagClickEventArgs(
                        sender as System.Windows.Documents.Hyperlink, 
                        ((sender as System.Windows.Documents.Hyperlink).Inlines.Last() as Run).Text));
            }
        }


    }

    public partial class HashtagClickEventArgs : EventArgs
    {
        public string Text { get; set; }
        public System.Windows.Documents.Hyperlink Hyperlink { get; set; }
        public HashtagClickEventArgs(System.Windows.Documents.Hyperlink Hyperlink, string Text)
        {
            this.Text = Text;
            this.Hyperlink = Hyperlink;
        }
    }
}
