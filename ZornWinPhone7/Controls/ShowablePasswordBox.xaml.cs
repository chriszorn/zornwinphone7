﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7.Controls
{
    public partial class ShowablePasswordBox : UserControl
    {
        public static readonly DependencyProperty PasswordTextProperty =
            DependencyProperty.Register("PasswordText", typeof(string), typeof(ShowablePasswordBox), new PropertyMetadata(default(string), PasswordTextPropertyChanged));

        public string PasswordText
        {
            get { return (string)GetValue(PasswordTextProperty); }
            set { SetValue(PasswordTextProperty, value); NotifyPropertyChanged(); }
        }

        private static void PasswordTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) { }

        private bool _isChecked = false;
        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; NotifyPropertyChanged(); CheckBox_IsChecked_Changed(); }
        }
        
        private Visibility _textBoxVisibility = System.Windows.Visibility.Collapsed;
        public Visibility TextBoxVisibility
        {
            get { return _textBoxVisibility; }
            set { _textBoxVisibility = value; NotifyPropertyChanged(); }
        }

        //public delegate void KeyUpEventHandler(object sender, System.Windows.Input.KeyEventArgs e);
        //public event KeyUpEventHandler KeyUp;

        public ShowablePasswordBox()
        {
            InitializeComponent();

            DataContext = this;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            (sender as PasswordBox).GetBindingExpression(PasswordBox.PasswordProperty).UpdateSource();
        }

        private void TextBox_PasswordChanged(object sender, TextChangedEventArgs e)
        {
            (sender as TextBox).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void TextBoxPassword_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            
        }

        private void CheckBox_IsChecked_Changed()
        {
            TextBoxVisibility = IsChecked ? Visibility.Visible : Visibility.Collapsed;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged([CallerMemberName] String info = "")
        {
            if (PropertyChanged != null)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
                catch { }
            }
        }
    }
}
