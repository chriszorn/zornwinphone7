﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Markup;
using Microsoft.Phone.Controls;

namespace ZornWinPhone7
{
    public class DayOfMonthPicker : ListPicker
    {
        public DayOfMonthPicker()
        {
            List<int> days = new List<int>();
            for (int i = 1; i <= 28; i++)
            {
                days.Add(i);
            }
            ItemsSource = days;

            string fullModeItemTemplateXaml =
            @"<DataTemplate
                xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                <Grid>
                    <TextBlock Text='{Binding}' FontSize='50' Margin='0,5' />
                </Grid>        
            </DataTemplate>";

            FullModeItemTemplate = (DataTemplate)XamlReader.Load(fullModeItemTemplateXaml);
            FullModeHeader = "CHOOSE DAY OF THE MONTH";
        }

        public void BindSelectedItem(object source, string path)
        {
            Binding binding = new Binding(path);
            binding.Source = source;
            binding.Mode = BindingMode.TwoWay;
            this.SetBinding(ListPicker.SelectedItemProperty, binding);
        }
    }
}
