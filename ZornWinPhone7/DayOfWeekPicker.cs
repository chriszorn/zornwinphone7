﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Markup;
using Microsoft.Phone.Controls;

namespace ZornWinPhone7
{
    public class DayOfWeekPicker : ListPicker
    {
        public DayOfWeekPicker()
        {
            List<DayOfWeek> days = new List<DayOfWeek>();
            for (int i = 0; i < 7; i++)
            {
                days.Add((DayOfWeek)i);
            }
            ItemsSource = days;

            string fullModeItemTemplateXaml =
            @"<DataTemplate
                xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                <Grid>
                    <TextBlock Text='{Binding}' FontSize='50' Margin='0,5' />
                </Grid>        
            </DataTemplate>";

            FullModeItemTemplate = (DataTemplate)XamlReader.Load(fullModeItemTemplateXaml);
            FullModeHeader = "CHOOSE DAY OF THE WEEK";
        }
    }
}
