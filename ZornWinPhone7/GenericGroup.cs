﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ZornWinPhone7
{
    public class GenericGroup<T> : ObservableSortedCollection<T>
    {
        public object Key
        {
            get;
            set;
        }

        public GenericGroup()
        {
            this.Key = string.Empty;
        }

        public GenericGroup(object name, IEnumerable<T> items)
        {
            this.Key = name;

            foreach (T item in items)
                this.Add(item);
        }
    }
}
