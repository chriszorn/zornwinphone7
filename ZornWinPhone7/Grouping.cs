﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZornWinPhone7
{
    public class Grouping<TKey, TElement> : IGrouping<TKey, TElement>
    {

        readonly List<TElement> elements;

        public Grouping(IGrouping<TKey, TElement> grouping)
        {
            Key = grouping.Key;
            elements = grouping.ToList();
        }

        public Grouping(TKey key, List<TElement> elements)
        {
            Key = key;
            this.elements = elements;
        }

        public TKey Key { get; private set; }

        public IEnumerator<TElement> GetEnumerator()
        {
            return this.elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    }
}
