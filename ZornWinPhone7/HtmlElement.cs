﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization;

namespace ZornWinPhone7
{
    [DataContract]
    public class HtmlElement : ModelBase
    {
        private string _objectClassName;
        [DataMember]
        public string ObjectClassName
        {
            get { return _objectClassName; }
            set { _objectClassName = value; NotifyPropertyChanged(); }
        }

        private string _source;
        [DataMember]
        public string Source
        {
            get { return _source; }
            set { _source = value; NotifyPropertyChanged(); }
        }

        private string _text;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = HttpUtility.HtmlDecode(value); NotifyPropertyChanged(); }
        }

        private string _url;
        [DataMember]
        public string Url
        {
            get { return _url; }
            set { _url = value; NotifyPropertyChanged(); }
        }

        private string _tag;
        [DataMember]
        public string Tag
        {
            get { return _tag; }
            set { _tag = value; NotifyPropertyChanged(); }
        }

        private string _class;
        [DataMember]
        public string Class
        {
            get { return _class; }
            set { _class = value; NotifyPropertyChanged(); }
        }

        private HtmlElement[] _inlineElements;
        [DataMember]
        public HtmlElement[] InlineElements
        {
            get { return _inlineElements; }
            set { _inlineElements = value; NotifyPropertyChanged(); }
        }
    }
}
