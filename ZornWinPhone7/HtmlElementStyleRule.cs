﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace ZornWinPhone7
{
    public class HtmlElementStyleRule
    {
        public string Tag = string.Empty;
        public string Class = string.Empty;
        public FontStyle FontStyle;
        public double FontSize;
        public FontWeight FontWeight;
        public string FontFamily;
        public Brush Foreground;

        public void Apply(ref Run run)
        {
            if (FontStyle != null)
            {
                run.FontStyle = FontStyle;
            }
            if (FontWeight != null)
            {
                run.FontWeight = FontWeight;
            }
            if (FontSize != 0)
            {
                run.FontSize = FontSize;
            }
            if (!string.IsNullOrWhiteSpace(FontFamily))
            {
                run.FontFamily = new FontFamily(FontFamily);
            }
            if (Foreground != null)
            {
                run.Foreground = Foreground;
            }
        }

        public void Apply(ref Paragraph paragraph)
        {
            if (FontStyle != null)
            {
                paragraph.FontStyle = FontStyle;
            }
            if (FontWeight != null)
            {
                paragraph.FontWeight = FontWeight;
            }
            if (FontSize != 0)
            {
                paragraph.FontSize = FontSize;
            }
            if (!string.IsNullOrWhiteSpace(FontFamily))
            {
                paragraph.FontFamily = new FontFamily(FontFamily);
            }
            if (Foreground != null)
            {
                paragraph.Foreground = Foreground;
            }
        }
    }

    
}
