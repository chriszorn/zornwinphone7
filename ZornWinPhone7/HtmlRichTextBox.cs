﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZornWinPhone7
{
    public class HtmlRichTextBox : RichTextBox
    {
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(HtmlElement[]), typeof(RichTextBox), new PropertyMetadata(new HtmlElement[] { }, ContentPropertyChanged));

        public HtmlElement[] Content
        {
            get { return (HtmlElement[])GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty StyleRulesProperty =
            DependencyProperty.Register("StyleRules", typeof(HtmlElementStyleRule[]), typeof(RichTextBox), new PropertyMetadata(new HtmlElementStyleRule[] { }, StyleRulesPropertyChanged));

        public HtmlElementStyleRule[] StyleRules
        {
            get { return (HtmlElementStyleRule[])GetValue(StyleRulesProperty); }
            set { SetValue(StyleRulesProperty, value); }
        }

        public static readonly DependencyProperty IsArticleSpacedProperty =
            DependencyProperty.Register("IsArticleSpaced", typeof(bool), typeof(RichTextBox), new PropertyMetadata(false, IsArticleSpacedPropertyChanged));

        public bool IsArticleSpaced
        {
            get { return (bool)GetValue(IsArticleSpacedProperty); }
            set { SetValue(IsArticleSpacedProperty, value); }
        }

        public static readonly DependencyProperty IsImageEnabledProperty =
            DependencyProperty.Register("IsImageEnabled", typeof(bool), typeof(RichTextBox), new PropertyMetadata(true, IsImageEnabledPropertyChanged));

        public bool IsImageEnabled
        {
            get { return (bool)GetValue(IsImageEnabledProperty); }
            set { SetValue(IsImageEnabledProperty, value); }
        }

        private const int maxImageHeight = 180;

        private static void ContentPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;
            HtmlElement[] content = dependencyPropertyChangedEventArgs.NewValue as HtmlElement[];

            Update(richTextBox, content, richTextBox.StyleRules);
        }

        private static void StyleRulesPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;
            HtmlElementStyleRule[] styles = dependencyPropertyChangedEventArgs.NewValue as HtmlElementStyleRule[];

            Update(richTextBox, richTextBox.Content, styles);
        }

        private static void IsArticleSpacedPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;

            Update(richTextBox, richTextBox.Content, richTextBox.StyleRules);
        }

        private static void IsImageEnabledPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var richTextBox = (HtmlRichTextBox)dependencyObject;

            Update(richTextBox, richTextBox.Content, richTextBox.StyleRules);
        }

        private static void Update(HtmlRichTextBox richTextBox, HtmlElement[] content, HtmlElementStyleRule[] styles)
        {
            richTextBox.Blocks.Clear();

            foreach (Block b in HtmlElementsToRichTextBox(content, styles, richTextBox.IsImageEnabled))
            {
                richTextBox.Blocks.Add(b);

                if (richTextBox.IsArticleSpaced)
                {
                    richTextBox.Blocks.Add(new Paragraph());
                }
            }
        }

        private static List<Block> HtmlElementsToRichTextBox(HtmlElement[] elements, HtmlElementStyleRule[] styles, bool showImages)
        {
            if (elements == null)
            {
                return new List<Block>();
            }

            List<Block> blocks = new List<Block>();
            string lastTag = string.Empty;

            Paragraph paragraph = new Paragraph();            

            foreach (HtmlElement element in elements)
            {
                if (IsTextElement(element))
                {
                    Run run = new Run();

                    //string[] lines = element.Text.Split(new string[] { "\\n" }, StringSplitOptions.None);
                    //if (lines.Length > 1)
                    //{
                    //    for(int i = 1; i < lines.Length; i++)
                    //    {
                    //        lines[i] = lines[i].TrimStart();
                    //    }
                    //}
                    //run.Text = string.Join(Environment.NewLine, lines);

                    if (element.Tag == "br")
                    {
                        run.Text = Environment.NewLine;
                    }
                    else
                    {
                        run.Text = element.Text;
                        if (lastTag == "br")
                        {
                            run.Text = run.Text.TrimStart();
                        }
                    }

                    ApplyStyles(styles, element, ref run);

                    if (element.ObjectClassName == "BoldText")
                    {
                        run.FontWeight = FontWeights.Bold;
                    }
                    else if (element.ObjectClassName == "ItalicText")
                    {
                        run.FontStyle = FontStyles.Italic;
                    }

                    paragraph.Inlines.Add(run);
                }
                else if (element.ObjectClassName == "TextLink")
                {
                    Hyperlink a = new Hyperlink();
                    try
                    {
                        a.NavigateUri = new Uri(element.Url);
                    }
                    catch (Exception) { }

                    a.TargetName = "_blank";
                    a.Inlines.Add(
                        new Run() 
                        { 
                            Text = HttpUtility.HtmlDecode(element.Text),
                            Foreground = Application.Current.Resources["PhoneAccentBrush"] as SolidColorBrush 
                        });

                    paragraph.Inlines.Add(a);
                }
                else if (element.ObjectClassName == "Image" && showImages)
                {
                    InlineUIContainer inline = new InlineUIContainer();
                    Image image = new Image()
                    {
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(0, 6, 0, 6),
                        Stretch = Stretch.Uniform,
                    };

                    try
                    {
                        image.Source = new BitmapImage() { UriSource = new Uri(element.Source) };
                    }
                    catch (Exception) { }

                    image.ImageOpened += image_ImageOpened;

                    inline.Child = image;

                    paragraph.Inlines.Add(inline);
                    paragraph.TextAlignment = TextAlignment.Center;
                }

                lastTag = element.Tag;
            }

            blocks.Add(paragraph);

            return blocks;
        }

        private static bool IsTextElement(HtmlElement element)
        {
            return element.ObjectClassName == "Text"
                || element.ObjectClassName == "BoldText"
                || element.ObjectClassName == "ItalicText";
        }

        private static void ApplyStyles(HtmlElementStyleRule[] styles, HtmlElement element, ref Run run)
        {
            if (styles != null && styles.Count() > 0)
            {
                var matchingStyle = styles.Where(x => x.Tag == element.Tag && x.Class == element.Class).FirstOrDefault();
                if (matchingStyle != null)
                {
                    matchingStyle.Apply(ref run);
                }
            }
        }

        static void image_ImageOpened(object sender, RoutedEventArgs e)
        {
            // Set the maxwidth of the image to the actual width of the image.
            // This prevents the image from being scaled up and becoming blurry
            double pixelWidth = ((sender as Image).Source as BitmapImage).PixelWidth;
            (sender as Image).MaxWidth = pixelWidth;
        }
    }
}
