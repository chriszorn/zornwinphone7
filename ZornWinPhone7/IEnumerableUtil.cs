﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZornWinPhone7
{
    public static class IEnumerableUtil
    {
        public static void SortObservableCollection<T>(ObservableCollection<T> enumerable, IEnumerable<T> sortedEnumerable)
        {
            int i = 0;
            for (i = 0; i < sortedEnumerable.Count(); i++)
            {
                if (enumerable.Count > i)
                {
                    if (!enumerable[i].Equals(sortedEnumerable.ElementAt(i)))
                    {
                        enumerable.RemoveAt(i);
                        enumerable.Insert(i, sortedEnumerable.ElementAt(i));
                    }
                }
                else
                {
                    enumerable.Add(sortedEnumerable.ElementAt(i));
                }
            }

            // Get rid of remaining items that don't belong
            while (i < enumerable.Count)
            {
                enumerable.RemoveAt(i);
            }
        }
        
        public static void UpdateSortedObservableCollection(ref ObservableCollection<IUpdateable> enumerable, ObservableCollection<IUpdateable> other)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException("enumerable");
            }

            if (other == null)
            {
                throw new ArgumentNullException("other");
            }

            // Iterate both lists. Update items that are 
            int i = 0;
            for (i = 0; i < other.Count; i++)
            {
                if (i < enumerable.Count)
                {
                    IUpdateable newItem = other.ElementAt(i);
                    IUpdateable currentItem = enumerable.ElementAt(i);

                    var matchingItem = enumerable.Where(x => x.Equals(newItem)).FirstOrDefault();

                    if (matchingItem != null)
                    {
                        matchingItem.Update(newItem);

                        // If the item at index i is not the one it is supposed to be,
                        // move the matching item into it's position
                        if (!currentItem.Equals(matchingItem))
                        {
                            enumerable.Remove(matchingItem);
                            enumerable.Insert(i, matchingItem);
                        }
                    }

                    // Item isn't in list
                    else
                    {
                        enumerable.Insert(i, newItem);
                    }
                }
            }

            // Remove remaining items that are no longer in the list
            while (enumerable.Count > other.Count)
            {
                enumerable.RemoveAt(enumerable.Count - 1);

            }
        }
    }
}
