﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZornWinPhone7
{
    public interface IUpdateable
    {
        void Update(object other);
    }
}
