﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ImageTools;

namespace ZornWinPhone7
{
    public static class IsolatedStorage
    {
        public static void WriteBitmapImageToFile(string fileName, BitmapImage bitmapImage)
        {
            IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            if (isolatedStorage.FileExists(fileName))
            {
                isolatedStorage.DeleteFile(fileName);
            }

            try
            {
                WriteableBitmap writableBitmap = new WriteableBitmap(bitmapImage);

                using (IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    writableBitmap.SaveJpeg(isolatedStorageFileStream, writableBitmap.PixelWidth, writableBitmap.PixelHeight, 0, 100);
                }
            }
            catch { }
        }

        public static void WriteUIElementToFile(string fileName, UIElement element, int width, int height)
        {
            IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            if (isolatedStorage.FileExists(fileName))
            {
                isolatedStorage.DeleteFile(fileName);
            }

            WriteableBitmap wb = new WriteableBitmap(width, height);
            element.Arrange(new Rect(0, 0, width, height));

            wb.Render(element, new CompositeTransform());
            wb.Invalidate();

            using (IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
            {
                // must include 'using ImageTools;'

                var img = wb.ToImage();
                var encoder = new ImageTools.IO.Png.PngEncoder();
                encoder.Encode(img, isolatedStorageFileStream);
            }
        }
    }
}
