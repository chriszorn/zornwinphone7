﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ZornWinPhone7
{
    public static class Media
    {
        public static async Task<BitmapImage> GetBitmapFromUrl(string url)
        {
            try
            {
                WebClient client = new WebClient();
                var res = await client.OpenReadTaskAsync(new Uri(url, UriKind.Absolute));
                if (res != null)
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.SetSource(res);
                    return bitmap;
                }
            }
            catch { }

            return null;
        }
    }
}
