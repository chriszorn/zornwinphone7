﻿using System;
using System.Net;

namespace ZornWinPhone7.Net
{
    public class NetResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Content { get; set; }
        public HttpWebResponse Response { get; set; }
    }
}
