
<toolkit:PhoneTextBox
	InputScope="Search"
	KeyUp="Search_KeyUp"
	Text="{Binding SearchUrl, Mode=TwoWay, UpdateSourceTrigger=Explicit}"
	TextChanged="TextBox_TextChanged"
	Hint="Enter a buzzfeed URL" />


	
    private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        (sender as TextBox).GetBindingExpression(TextBox.TextProperty).UpdateSource();
    }
    
    
    
    private void Search_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
    {
        if (e.Key == System.Windows.Input.Key.Enter)
        {
            (DataContext as ViewModels.HomeViewModel).SearchUrl_SearchClick();
            this.Focus();
        }
    }