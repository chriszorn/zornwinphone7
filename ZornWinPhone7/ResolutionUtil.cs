﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZornWinPhone7
{
    public static class ResolutionUtil
    {
        public enum Resolutions
        {
            WVGA,
            WXGA,
            HD720p
        }

        static int? scaleFactor;

        static ResolutionUtil()
        {
            object scaleFactorValue = GetPropertyValue(System.Windows.Application.Current.Host.Content, "ScaleFactor");
            if (scaleFactorValue != null)
            {
                scaleFactor = Convert.ToInt32(scaleFactorValue);
            }
        }

        public static Resolutions CurrentResolution
        {
            get
            {
                switch (scaleFactor)
                {
                    case 160:
                        return Resolutions.WXGA;

                    case 150:
                        return Resolutions.HD720p;
                }

                return Resolutions.WVGA;
            }
        }

        private static object GetPropertyValue(object instance, string name)
        {
            try
            {
                return instance.GetType().GetProperty(name).GetValue(instance, null);
            }
            catch
            {
                return null;
            }
        }
    }
}
