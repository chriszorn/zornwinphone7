﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Tasks;
using WindowsPhoneApp;

namespace ZornWinPhone7
{
    public static class ReviewPrompt
    {
        //private static readonly Setting<bool> _reviewPrompInit = new Setting<bool>("zwp-ReviewPrompInit", false);
        private static readonly Setting<bool> _isEnabled = new Setting<bool>("zwp-ReviewPrompt-IsEnabled", true);
        private static readonly Setting<int> _appOpenCount = new Setting<int>("zwp-ReviewPrompt-AppOpenCount", 0);
        private static readonly Setting<int> _delay = new Setting<int>("zwp-ReviewPrompt-Delay", 0);
        private static readonly Setting<int> _minNumOpenings = new Setting<int>("zwp-ReviewPrompt-MinNumOpenings", 30);
        private static readonly Setting<int> _lastSnooze = new Setting<int>("zwp-ReviewPrompt-LastSnooze", 0);
        private static readonly Setting<int> _snoozeLength = new Setting<int>("zwp-ReviewPrompt-SnoozeLength", 30);

        private static MessagePrompt _prompt = null;

        public static void Init(int minNumAppOpenings, int snoozeLength, int popupDelayInSeconds)
        {
            _minNumOpenings.Value = minNumAppOpenings;
            _snoozeLength.Value = snoozeLength;
            _delay.Value = popupDelayInSeconds;
        }

        public static async void OnAppOpen()
        {
            if (_isEnabled.Value)
            {
                _appOpenCount.Value++;

                if (_appOpenCount.Value >= _minNumOpenings.Value
                    && (_lastSnooze.Value == 0 || _appOpenCount.Value >= _lastSnooze.Value + _delay.Value))
                {

                    if (_delay.Value > 0)
                    {
                        await TaskEx.Run(() => System.Threading.Thread.Sleep(_delay.Value * 1000));
                    }

                    try
                    {
                        System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                var message = new TextBlock()
                                {
                                    Text = "If you enjoy this app, please positively review the app.",
                                    TextWrapping = System.Windows.TextWrapping.Wrap,
                                    Margin = new System.Windows.Thickness(0, 20, 0, 40),
                                    Foreground = System.Windows.Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush,

                                };

                                var contactAuthor = new HyperlinkButton()
                                {
                                    Content = "Contact app author",
                                    HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                                    Foreground = System.Windows.Application.Current.Resources["PhoneSubtleBrush"] as SolidColorBrush,
                                    Margin = new System.Windows.Thickness(-12, 0, 0, 0),
                                    FontSize = 18,
                                };

                                var hideForever = new Button()
                                {
                                    Content = "Don't ask again",
                                    HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
                                    BorderBrush = System.Windows.Application.Current.Resources["PhoneSubtleBrush"] as SolidColorBrush,
                                    Foreground = System.Windows.Application.Current.Resources["PhoneSubtleBrush"] as SolidColorBrush,
                                    FontSize = 15,
                                };

                                hideForever.Click += hideForever_Click;
                                contactAuthor.Click += contactAuthor_Click;

                                var grid = new Grid();
                                grid.Children.Add(contactAuthor);
                                grid.Children.Add(hideForever);

                                var body = new StackPanel();
                                body.Children.Add(message);
                                body.Children.Add(grid);

                                _prompt = new MessagePrompt()
                                    {
                                        Title = "Rate this app!",
                                        Body = body,
                                    };

                                var okayButton = new Button()
                                    {
                                        Content = "Okay!",
                                        Background = System.Windows.Application.Current.Resources["PhoneAccentBrush"] as SolidColorBrush,
                                        Foreground = new SolidColorBrush() { Color = Colors.White },
                                    };

                                okayButton.Click += okayButton_Click;

                                var laterButton = new Button()
                                    {
                                        Content = "Later",
                                    };

                                laterButton.Click += laterButton_Click;

                                _prompt.ActionPopUpButtons.Clear();
                                _prompt.ActionPopUpButtons.Add(okayButton);
                                _prompt.ActionPopUpButtons.Add(laterButton);
                                _prompt.Show();
                            }
                            catch { }
                        });
                    }
                    catch { }
                }
            }
        }

        static void hideForever_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_prompt != null)
            {
                _prompt.Hide();
            }

            _isEnabled.Value = false;
        }

        static void contactAuthor_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.To = "chris.zorn@live.com";
            emailComposeTask.Body = "";
            emailComposeTask.Subject = "Windows Phone App Support";
            emailComposeTask.Show();
        }

        static void laterButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_prompt != null)
            {
                _prompt.Hide();
            }

            _lastSnooze.Value = _appOpenCount.Value;
        }

        static void okayButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_prompt != null)
            {
                _prompt.Hide();
            }

            _isEnabled.Value = false;

            ZornWinPhone7.Tasks.RateApp();
        }
    }
}
