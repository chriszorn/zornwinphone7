﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ZornWinPhone7
{
    public class SaveDiscardCancelPage : PhoneApplicationPage
    {
        public SaveDiscardCancelPage()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBar.Buttons.Add(new ApplicationBarIconButton() { Text = "save", IconUri = new Uri("/Assets/Icons/check.png", UriKind.Relative), IsEnabled = true });
            ApplicationBar.Buttons.Add(new ApplicationBarIconButton() { Text = "discard", IconUri = new Uri("/Assets/Icons/delete.png", UriKind.Relative), IsEnabled = true });
            ApplicationBar.Buttons.Add(new ApplicationBarIconButton() { Text = "cancel", IconUri = new Uri("/Assets/Icons/cancel.png", UriKind.Relative), IsEnabled = true });

            this.Loaded += SaveDiscardCancelPage_Loaded;
        }

        void SaveDiscardCancelPage_Loaded(object sender, RoutedEventArgs e)
        {
            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Click -= (DataContext as SaveDiscardCancelViewModel).SaveButton_Click;
            (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Click -= (DataContext as SaveDiscardCancelViewModel).DiscardButton_Click;
            (ApplicationBar.Buttons[2] as ApplicationBarIconButton).Click -= (DataContext as SaveDiscardCancelViewModel).CancelButton_Click;

            (ApplicationBar.Buttons[0] as ApplicationBarIconButton).Click += (DataContext as SaveDiscardCancelViewModel).SaveButton_Click;
            (ApplicationBar.Buttons[1] as ApplicationBarIconButton).Click += (DataContext as SaveDiscardCancelViewModel).DiscardButton_Click;
            (ApplicationBar.Buttons[2] as ApplicationBarIconButton).Click += (DataContext as SaveDiscardCancelViewModel).CancelButton_Click;
        }
    }
}