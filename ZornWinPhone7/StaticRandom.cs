﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/* http://stackoverflow.com/questions/767999/random-number-generator-only-generating-one-random-number */

namespace ZornWinPhone7
{
    public static class StaticRandom
    {
        //Function to get random number
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }
    }
}
