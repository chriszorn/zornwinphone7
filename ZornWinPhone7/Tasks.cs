﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace ZornWinPhone7
{
    public static class Tasks
    {
        public static void OpenInBrowser(string url)
        {
            try
            {
                OpenInBrowser(new Uri(url));
            }
            catch { }
        }

        public static void OpenInBrowser(Uri uri)
        {
            try
            {
                Microsoft.Phone.Tasks.WebBrowserTask task = new Microsoft.Phone.Tasks.WebBrowserTask();
                task.Uri = uri;
                task.Show();
            }
            catch { }
        }

        public static void RateApp()
        {
            try
            {
                (new Microsoft.Phone.Tasks.MarketplaceReviewTask()).Show();
            }
            catch { }
        }

        public static bool SaveImageToPicturesHub(string fileName, BitmapImage bitmapImage)
        {
            if (bitmapImage == null)
            {
                return false;
            }

            try
            {
                WriteableBitmap writableBitmap = new WriteableBitmap(bitmapImage);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    writableBitmap.SaveJpeg(memoryStream, writableBitmap.PixelWidth, writableBitmap.PixelHeight, 0, 100);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    var mediaLibrary = new Microsoft.Xna.Framework.Media.MediaLibrary();
                    mediaLibrary.SavePicture(fileName, memoryStream.GetBuffer());
                }
            }
            catch { return false; }

            return true;
        }

        public static bool SaveImageToPicturesHub(string fileName, System.Windows.Controls.Image image)
        {
            return SaveImageToPicturesHub(fileName, image.Source as BitmapImage);
        }
    }
}
