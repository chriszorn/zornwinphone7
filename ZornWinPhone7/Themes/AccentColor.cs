﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ZornWinPhone7.Themes
{
    public class AccentColor
    {
        public string Name { get; set; }
        public Color Color { get; set; }

        public SolidColorBrush SolidColorBrush
        {
            get { return new SolidColorBrush() { Color = Color }; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            AccentColor other = obj as AccentColor;

            if (other != null)
            {
                return Color.Equals(other.Color);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Color.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
