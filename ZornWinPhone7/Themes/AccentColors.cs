﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZornWinPhone7.Themes
{
    public class AccentColors
    {
        public static List<AccentColor> CustomColors { get; set; }

        public static List<AccentColor> All
        {
            get
            {
                var colors = new List<AccentColor>(CustomColors);
                colors.AddRange(new List<AccentColor>()
                    {
                        Lime,
                        Green,
                        Emerald,
                        Teal,
                        Cyan,
                        Cobalt,
                        Indigo,
                        Violet,
                        Pink,
                        Magenta,
                        Crimson,
                        Red,
                        Orange,
                        Amber,
                        Brown,
                        Olive,
                        Steel,
                        Mauve,
                        Taupe,
                    });

                return colors;
            }
        }

        static AccentColors()
        {
            CustomColors = new List<AccentColor>();
        }

        public static AccentColor Lime = new AccentColor()
        {
            Name = "Lime",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#A4C400"),
        };
        public static AccentColor Green = new AccentColor()
        {
            Name = "Green",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#60A917"),
        };
        public static AccentColor Emerald = new AccentColor()
        {
            Name = "Emerald",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#008A00"),
        };
        public static AccentColor Teal = new AccentColor()
        {
            Name = "Teal",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#00ABA9"),
        };
        public static AccentColor Cyan = new AccentColor()
        {
            Name = "Cyan",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#1BA1E2"),
        };
        public static AccentColor Cobalt = new AccentColor()
        {
            Name = "Cobalt",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#0050EF"),
        };
        public static AccentColor Indigo = new AccentColor()
        {
            Name = "Indigo",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#6A00FF"),
        };
        public static AccentColor Violet = new AccentColor()
        {
            Name = "Violet",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#AA00FF"),
        };
        public static AccentColor Pink = new AccentColor()
        {
            Name = "Pink",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#F472D0"),
        };
        public static AccentColor Magenta = new AccentColor()
        {
            Name = "Magenta",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#D80073"),
        };
        public static AccentColor Crimson = new AccentColor()
        {
            Name = "Crimson",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#A20025"),
        };
        public static AccentColor Red = new AccentColor()
        {
            Name = "Red",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#E51400"),
        };
        public static AccentColor Orange = new AccentColor()
        {
            Name = "Orange",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#FA6800"),
        };
        public static AccentColor Amber = new AccentColor()
        {
            Name = "Amber",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#F0A30A"),
        };
        public static AccentColor Yellow = new AccentColor()
        {
            Name = "Yellow",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#E3C800"),
        };
        public static AccentColor Brown = new AccentColor()
        {
            Name = "Brown",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#825A2C"),
        };
        public static AccentColor Olive = new AccentColor()
        {
            Name = "Olive",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#6D8764"),
        };
        public static AccentColor Steel = new AccentColor()
        {
            Name = "Steel",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#647687"),
        };
        public static AccentColor Mauve = new AccentColor()
        {
            Name = "Mauve",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#76608A"),
        };
        public static AccentColor Taupe = new AccentColor()
        {
            Name = "Taupe",
            Color = ZornWinPhone7.ColorUtil.GetColorFromHex("#87794E"),
        };
    }
}
