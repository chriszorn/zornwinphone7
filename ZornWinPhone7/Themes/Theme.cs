﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZornWinPhone7.Themes
{
    public class Theme
    {
        //public AccentColor AccentColor { get; set; }
        public string Name { get; set; }
        public Dictionary<string, Color> ResourceColors;

        public Theme()
        {
            //AccentColor = AccentColors.Red;
            ResourceColors = new Dictionary<string, Color>();
        }

        public void Apply()
        {
            foreach (KeyValuePair<string, Color> pair in ResourceColors)
            {
                SetResource(string.Format("{0}Color", pair.Key), pair.Value);
                SetBrushColor(string.Format("{0}Brush", pair.Key), pair.Value);
            }
        }

        public void UpdateApplicationBar(Microsoft.Phone.Shell.IApplicationBar appBar)
        {
            if (ResourceColors.ContainsKey("PhoneChrome"))
            {
                appBar.BackgroundColor = ResourceColors["PhoneChrome"];
            }

            if (ResourceColors.ContainsKey("PhoneForeground"))
            {
                appBar.ForegroundColor = ResourceColors["PhoneForeground"];
            }
        }

        public static void SetResource(string name, object value)
        {
            if (Application.Current.Resources.Contains(name))
            {
                Application.Current.Resources.Remove(name);
            }

            Application.Current.Resources.Add(name, value);

            System.Diagnostics.Debug.WriteLine("SetResource: {{{0}, {1}}}", name, Convert.ToString(value));
        }

        public static void SetBrushColor(string name, Color color)
        {
            if (Application.Current.Resources.Contains(name))
            {
                (Application.Current.Resources[name] as SolidColorBrush).Color = color;
            }
            else
            {
                Application.Current.Resources.Add(name, new SolidColorBrush() { Color = color });
            }

            System.Diagnostics.Debug.WriteLine("SetResource: {{{0}, {1}}}", name, Convert.ToString(color));
        }

        public override bool Equals(object obj)
        {
            return Name.Equals(((Theme)obj).Name);
        }
    }
}
