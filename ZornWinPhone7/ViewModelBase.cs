﻿using System;
using System.Net;
using System.Windows;
using System.ComponentModel;
using Microsoft.Phone.Controls;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

namespace ZornWinPhone7
{
    public class ViewModelBase : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged([CallerMemberName] String info = "")
        {
            if (PropertyChanged != null)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
                catch { }
            }
        }

        private bool _progressIndicatorIsVisible;
        public bool ProgressIndicatorIsVisible
        {
            get { return _progressIndicatorIsVisible; }
            set { _progressIndicatorIsVisible = value; NotifyPropertyChanged(); }
        }

        private string _progressIndicatorText;
        public string ProgressIndicatorText
        {
            get { return _progressIndicatorText; }
            set { _progressIndicatorText = value; NotifyPropertyChanged(); }
        }

        private double _systemTrayOpacity = 1.0;
        public double SystemTrayOpacity
        {
            get { return _systemTrayOpacity; }
            set { _systemTrayOpacity = value; NotifyPropertyChanged(); }
        }

        private bool _systemTrayIsVisible = true;
        public bool SystemTrayIsVisible
        {
            get { return _systemTrayIsVisible; }
            set { _systemTrayIsVisible = value; NotifyPropertyChanged(); }
        }

        public bool ShowLoadingMessage { get; set; }
        public BitmapImage ToastImage { get; set; }
        public string ToastTitle { get; set; }

        private string progressIndicatorLoadingText = "Loading...";
        private string lastProgressIndicatorText = string.Empty;
        private double? lastSystemTrayOpacity = null;
        private bool? lastSystemTrayIsVisible = null;
        private bool promptIsVisible = false;

        public ViewModelBase()
        {
            ShowLoadingMessage = true;
            ToastTitle = string.Empty;
        }

        public void ShowProgressBar()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ProgressIndicatorIsVisible = true;

                if (ShowLoadingMessage)
                {
                    StashProgressIndicatorText();
                }
            });
        }

        public void HideProgresBar()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ProgressIndicatorIsVisible = false;

                if (ShowLoadingMessage)
                {
                    RestoreProgressIndicatorText();
                }
            });
        }

        public void ShowToast(string message)
        {
            ShowToast(ToastTitle, message, System.Windows.Controls.Orientation.Horizontal, false);
        }

        public void ShowToast(string message, bool textWrap)
        {
            ShowToast(ToastTitle, message, System.Windows.Controls.Orientation.Vertical, true);
        }

        public void ShowToast(string title, string message)
        {
            ShowToast(title, message, System.Windows.Controls.Orientation.Horizontal, false);
        }

        public void ShowToast(string title, string message, System.Windows.Controls.Orientation textOrientation, bool textWrap)
        {
            var prompt = new Coding4Fun.Toolkit.Controls.ToastPrompt();
            prompt.TextOrientation = textOrientation;

            if (!string.IsNullOrEmpty(title))
            {
                prompt.Title = title;
            }

            if (!string.IsNullOrEmpty(message))
            {
                prompt.Message = message;
            }

            if (ToastImage != null)
            {
                prompt.ImageSource = ToastImage;
            }

            if (textWrap)
            {
                prompt.TextWrapping = TextWrapping.Wrap;
            }

            StashProgressIndicatorText();
            StashSystemTrayIsVisible();
            StashSystemTrayOpacity();

            ProgressIndicatorText = string.Empty;

            if (lastSystemTrayOpacity != 1)
            {
                SystemTrayIsVisible = false;
            }

            System.Threading.ThreadPool.QueueUserWorkItem((state) =>
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {

                    prompt.Completed += (sender, e) =>
                        {
                            promptIsVisible = false;

                            RestoreSystemTrayOpacity();
                            RestoreSystemTrayIsVisible();
                            RestoreProgressIndicatorText();
                        };

                    promptIsVisible = true;

                    try
                    {
                        prompt.Show();
                    }
                    catch { }
                });
            });
        }

        private void StashProgressIndicatorText()
        {
            if (ProgressIndicatorText != progressIndicatorLoadingText)
            {
                lastProgressIndicatorText = ProgressIndicatorText;
                ProgressIndicatorText = progressIndicatorLoadingText;
            }
        }

        private void RestoreProgressIndicatorText()
        {
            if (!promptIsVisible)
            {
                ProgressIndicatorText = lastProgressIndicatorText;
            }
        }

        private void StashSystemTrayOpacity()
        {
            if (lastSystemTrayOpacity == null)
            {
                lastSystemTrayOpacity = SystemTrayOpacity;
            }
        }

        private void RestoreSystemTrayOpacity()
        {
            if (lastSystemTrayOpacity != null)
            {
                SystemTrayOpacity = (double)lastSystemTrayOpacity;
                lastSystemTrayOpacity = null;
            }
        }

        private void StashSystemTrayIsVisible()
        {
            if (lastSystemTrayIsVisible == null)
            {
                lastSystemTrayIsVisible = SystemTrayIsVisible;
            }
        }

        private void RestoreSystemTrayIsVisible()
        {
            if (lastSystemTrayIsVisible != null)
            {
                SystemTrayIsVisible = (bool)lastSystemTrayIsVisible;
                lastSystemTrayIsVisible = null;
            }
        }

        protected void GoBack()
        {
            if ((Application.Current.RootVisual as PhoneApplicationFrame).CanGoBack)
            {
                (Application.Current.RootVisual as PhoneApplicationFrame).GoBack();
            }
        }
    }
}
