﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZornWinPhone7.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        private ObservableCollection<AboutItem> _aboutItems;
        public ObservableCollection<AboutItem> AboutItems
        {
            get { return _aboutItems; }
            set { _aboutItems = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<AppMenuItem> _apps;
        public ObservableCollection<AppMenuItem> Apps
        {
            get { return _apps; }
            set { _apps = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<AppMenuItem> _credits;
        public ObservableCollection<AppMenuItem> Credits
        {
            get { return _credits; }
            set { _credits = value; NotifyPropertyChanged(); }
        }

        private string _applicationTitle = string.Empty;
        public string ApplicationTitle
        {
            get { return _applicationTitle; }
            set { _applicationTitle = value; NotifyPropertyChanged(); }
        }

        private string _applicationImage = string.Empty;
        public string ApplicationImage
        {
            get { return _applicationImage; }
            set { _applicationImage = value; NotifyPropertyChanged(); }
        }

        private string _applicationStorePageUrl = string.Empty;
        public string ApplicationStorePageUrl
        {
            get { return _applicationStorePageUrl; }
            set { _applicationStorePageUrl = value; AddUpdateButton();  NotifyPropertyChanged(); }
        }

        private string _applicationUserVoiceUrl = string.Empty;
        public string ApplicationUserVoiceUrl
        {
            get { return _applicationUserVoiceUrl; }
            set { _applicationUserVoiceUrl = value; NotifyPropertyChanged(); }
        }

        private string _version = string.Empty;
        public string Version
        {
            get { return _version; }
            set { _version = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<AppMenuItem> _options;
        public ObservableCollection<AppMenuItem> Options
        {
            get { return _options; }
            set { _options = value; NotifyPropertyChanged("AboutItems"); }
        }

        public System.Windows.Media.SolidColorBrush PhoneAccentBrushPressed
        {
            get
            {
                return new System.Windows.Media.SolidColorBrush() { Color = ZornWinPhone7.ColorUtil.ChangeColorBrightness((System.Windows.Application.Current.Resources["PhoneAccentBrush"] as System.Windows.Media.SolidColorBrush).Color, 0.3F) };
            }
        }

        private bool _isDarkTheme;
        public bool IsDarkTheme
        {
            get { return _isDarkTheme; }
            set { _isDarkTheme = value; NotifyPropertyChanged(); }
        }

        public static AppMenuItem CheckForUpdateItem = new AppMenuItem()
            {
                Title = "Check for Update",
                Text = "Check the app store for an app update",
                IconImage = "/ZornWinPhone7;component/Assets/Icons/update.png"
            };

        public static AppMenuItem RateItem = new AppMenuItem() 
            { 
                Title = "Rate & Review", 
                Text = "Rate this app in the Windows Phone Store", 
                IconImage = "/ZornWinPhone7;component/Assets/Icons/rate.png"
            };

        public static AppMenuItem WebsiteItem = new AppMenuItem()
        {
            Title = "zornchris.com",
            Text = "Contact me for support via my website",
            IconImage = "/ZornWinPhone7;component/Assets/Icons/browser.png",
            Destination = new Uri("http://zornchris.com", UriKind.Absolute),
        };

        public static AppMenuItem TwitterItem = new AppMenuItem() 
        { 
            Title = "@chris_zorn", 
            Text = "Contact me for support via twitter", 
            IconImage = "/ZornWinPhone7;component/Assets/Icons/twitter.png",
            Destination = new Uri("http://twitter.com/chris_zorn", UriKind.Absolute),
        };

        public static AppMenuItem EmailItem = new AppMenuItem()
        {
            Title = "chris.zorn@live.com",
            Text = "Contact me for support via email",
            IconImage = "/ZornWinPhone7;component/Assets/Icons/email.png"
        };

        public AboutViewModel()
        {
            SystemTrayIsVisible = true;
            SystemTrayOpacity = 1;

            Credits = new ObservableCollection<AppMenuItem>()
            {
                new AppMenuItem() { Title = "Eddi Bento", Destination = new Uri("http://windowsphone.com/"), Text = "Logos for pin.it, buzz.it, and Steam Chat" },
                new AppMenuItem() { Title = "Windows Phone Toolkit", Destination = new Uri("http://phone.codeplex.com/"), Text = "A collection of controls for Windows Phone" },
                new AppMenuItem() { Title = "Json.NET", Destination = new Uri("http://json.codeplex.com/"), Text = "Json.NET is a popular high-performance JSON framework for .NET" },
                new AppMenuItem() { Title = "Icons8", Destination = new Uri("http://icons8.com/"), Text = "The world largest original set of free icons for Windows8/Metro" },
                new AppMenuItem() { Title = "Little Watson", Destination = new Uri("http://blogs.msdn.com/b/andypennell/archive/2010/11/01/error-reporting-on-windows-phone-7.aspx"), Text = "Error Reporting for Windows Phone" },
                new AppMenuItem() { Title = "WP7 Data Visualization", Destination = new Uri("http://blogs.msdn.com/b/delay/archive/2010/04/20/phone-y-charts-silverlight-wpf-data-visualization-development-release-4-and-windows-phone-7-charting-sample.aspx"), Text = "A WP7 port of the .NET Data Visualization framework" },
                new AppMenuItem() { Title = "Kawagoe Toolkit", Destination = new Uri("http://kawagoe.codeplex.com/"), Text = "Persistent Image Caching for offline image viewing" },
                new AppMenuItem() { Title = "Ad Rotator", Destination = new Uri("http://wp7adrotator.codeplex.com/"), Text = "Use ads from multiple publishers in your app" },
            };

            Options = new ObservableCollection<AppMenuItem>()
            {
                RateItem,
                WebsiteItem,
                TwitterItem,
                EmailItem,
            };

            Apps = new ObservableCollection<AppMenuItem>()
            {
                new AppMenuItem() { Title = "pin.it", Text = "A full-featured Pinterest client - the best one around", IconImage = "/ZornWinPhone7;component/Assets/AppIcons/PinIt.png", Destination = new Uri("http://www.windowsphone.com/en-us/store/app/pin-it-pinterest/7aad351c-093d-494e-be80-0d052a078145", UriKind.Absolute) },
                new AppMenuItem() { Title = "buzz.it", Text = "Browse & share articles from BuzzFeed.com", IconImage = "/ZornWinPhone7;component/Assets/AppIcons/BuzzFeed.png", Destination = new Uri("http://www.windowsphone.com/en-us/store/app/buzz-it-buzzfeed/cd063f62-1349-48af-8ee5-9530429b9829", UriKind.Absolute) },
                new AppMenuItem() { Title = "Complete League of Legends", Text = "Everything you could ever need for League of Legends", IconImage = "/ZornWinPhone7;component/Assets/AppIcons/LeagueComplete.png", Destination = new Uri("http://www.windowsphone.com/en-us/store/app/complete-league-of-legends/88b11e28-c176-40fc-980d-5b6808d9c4cb", UriKind.Absolute) },
                new AppMenuItem() { Title = "HearthStone Plus", Text = "Deck builder & game information for HearthStone: Heroes of Warcraft", IconImage = "/ZornWinPhone7;component/Assets/AppIcons/HearthStone.png", Destination = new Uri("http://www.windowsphone.com/en-us/store/app/hearthstone-plus/5c522015-f300-4a93-9058-e5740f21039e", UriKind.Absolute) },
                new AppMenuItem() { Title = "The Onion News", Text = "View the latest articles from The Onion", IconImage = "/ZornWinPhone7;component/Assets/AppIcons/TheOnion.png", Destination = new Uri("http://www.windowsphone.com/en-us/store/app/the-onion-news/24bbd402-b263-4694-bae9-44fe8deff798", UriKind.Absolute) },
            };

            IsDarkTheme = ThemeUtil.IsDarkThemeEnabled;

            if (DesignerProperties.IsInDesignTool)
            {
                AboutItems = new ObservableCollection<AboutItem>()
                {
                    new AboutItem() { 
                        Version = "Version 1.1.0.0", 
                        Text = "- Fixed 'Out of Memory' errors\n- Added comments to articles"
                    },
                    new AboutItem() { 
                        Version = "Version 1.0.0.0", 
                        Text = "- Initial Release"
                    },
                };
                ApplicationTitle = "League Forums";
                Version = "4.2.3.0";
                ApplicationImage = "/Assets/ApplicationIcon.png";
                ApplicationStorePageUrl = "x";
            }
        }

        private void AddUpdateButton()
        {
            if (!Options.Contains(CheckForUpdateItem) && !string.IsNullOrWhiteSpace(ApplicationStorePageUrl))
            {
                try
                {
                    CheckForUpdateItem.Destination = new Uri(ApplicationStorePageUrl);
                    Options.Insert(0, CheckForUpdateItem);
                }
                catch { }
            }
        }

        internal void BugReportButton_Click()
        {
            if(!string.IsNullOrWhiteSpace(ApplicationUserVoiceUrl))
            {
                ZornWinPhone7.Tasks.OpenInBrowser(ApplicationUserVoiceUrl);
            }
        }
    }
}
