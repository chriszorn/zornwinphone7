﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ZornWinPhone7.ViewModels
{
    public class ColorPickerViewModel : ZornWinPhone7.ViewModelBase
    {
        private SolidColorBrush _fill = new SolidColorBrush(Colors.Blue);
        public SolidColorBrush Fill
        {
            get { return _fill; }
            set { _fill = value; NotifyPropertyChanged(); }
        }

        private string _header = "Test Header";
        public string Header
        {
            get { return _header; }
            set { _header = value; NotifyPropertyChanged(); }
        }
    }
}
