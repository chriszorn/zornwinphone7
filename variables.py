variables = """
List<AccentColor> CustomColors
"""

vars = variables.split('\n')

for v in vars:
	
	if(v == ''):
		continue;
	
	parts = v.split(' ')
	t = parts[0]
	n = parts[1]
	
	priv = '_%s%s' % (n[0].lower(), n[1:])
	pub = '%s%s' % (n[0].upper(), n[1:])
	
	o = """
        private %s %s;
        public %s %s
        {
            get { return %s; }
            set { %s = value; NotifyPropertyChanged(); }
        }""" % (t, priv, t, pub, priv, priv)
		
	print o